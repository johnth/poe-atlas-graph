# Cytoscape atlas:

## Selected list:
- Highlight maps under mouseover
- Select specific map on click
- Show number of inbound atlas paths
- Sorting / grouping

## Graph:
- show map information on mouseover
- make look and respond more like ingame map

## General:
- Clarify GGG asset licensing
- Split content, script, style
- Add [navigator][navigator]
- Add [adjust url with any selection modification][jsmodurl]
- Set map status by js / url: eg: `&complete=t<4`
- Store map status localStorage or cookie

## Extras:
- Sextant influence rings
- Pantheon bosses
- Additional edges from Content.ggpk data:
  - tileset
  - monster variety
- Completion / bonus completion indication

# data:
- Split into separate repo
- Automate Content.ggpk transform to usable data
  - triggered by PoE update

[//]: # (Sources)

[navigator]: https://github.com/cytoscape/cytoscape.js-navigator

[jsmodurl]: https://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page#3354511
