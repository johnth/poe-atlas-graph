"use strict";

const db = require("@arangodb").db;

const graph_module = require("@arangodb/general-graph");
const atlas = graph_module._graph("atlas");

const baseItemTypes = db._collection("BaseItemTypes");
const maps = db._collection("Maps");
const mapAtlasPaths = db._collection("Maps-edges-HigherTierMaps_BaseItemTypesKeys")
const mapVendorPaths = db._collection("Maps-edges-MapUpgrade_BaseItemTypesKey")


// Allow to both export functions and use internally
var _public = {};

/**
* Get the document IDs for basic details of a map.
* @param {string} The AtlasNode, BaseItemTypes, Maps or WorldAreas ID of a map.
* @return {array} Document IDs.
*/
_public.mapDetailsIDs = function(startNodeID) {
  var edgeMapDetailsEdges = [
    "AtlasNode-edges-WorldAreasKey",
    "Maps-edges-Regular_WorldAreasKey",
    "Maps-edges-Unique_WorldAreasKey",
    "Maps-edges-BaseItemTypesKey"
  ];

  var edgeMapDetailsOptions = {
    "direction": "any",
    "edgeCollectionRestriction": edgeMapDetailsEdges,
    "minDepth": 1,
    "maxDepth": 3
  };

  return atlas._neighbors(startNodeID, edgeMapDetailsOptions);
}

/**
 * Get an object from document IDs.
 * @param {array} The document IDs to merge into one object. 
 * @return {object} A collection-name as key based object.
 */
_public.documentIDsToObject = function(documentIDs) {
  var mapDetails = {}
  documentIDs.forEach(function(documentid) {
    var collectionName = documentid.split("/",1)[0];
    mapDetails[collectionName] = db._document(documentid);
  })
  return mapDetails;
}

/**
* Get the basic details of a map as an object.
* @param {string} The AtlasNode, BaseItemTypes, Maps or WorldAreas ID of a map.
* @return {object} A collection-name as key based object.
*/
_public.mapDetails = function(mapID) {
  var mapIDs = _public.mapDetailsIDs(mapID);
  var mapObject = _public.documentIDsToObject(mapIDs);
  return mapObject;
}

/**
 * Get map document IDs for a given series.
 * @param {number} The map series key to use (War = 4).
 * @return {array} Maps IDs of all maps in given series.
 */
_public.getAllMapIDsBySeries = function(mapSeriesKey) {
  var mapsSeriesExample = {"MapSeriesKey": mapSeriesKey};
  var mapIDs = [];
  maps.byExample(mapsSeriesExample).toArray().forEach(function(item) {
    mapIDs.push(item._id);
  });
  return mapIDs;
}

/**
 * Get all map objects for a given series.
 * @param {number} The map series key to use (War = 4).
 * @return {array} Maps objects.
 */
_public.getAllMapDetailObjectsBySeries = function(mapSeriesKey) {
  var mapObjects = [];
  var mapIDs = _public.getAllMapIDsBySeries(mapSeriesKey);
  mapIDs.forEach(function(item) {
    var mapDetails = _public.mapDetails(item);
    mapObjects.push(mapDetails);
  });
  return mapObjects;
}

/**
 * Get map relations for a given series and edges collection.
 * @param {number} The map series key to use (War = 4).
 * @param {string} The name of the edgeCollection. Atlas paths in "Maps-edges-HigherTierMaps_BaseItemTypesKeys", vendor upgrades in "Maps-edges-MapUpgrade_BaseItemTypesKey".
 * @return {array} Maps relation objects.
 */
_public.getAllEdgesBySeriesAndEdge = function(mapSeriesKey, edgeCollectionName) {
  var edges = [];
  db._collection(edgeCollectionName).edges(
    _public.getAllMapIDsBySeries(mapSeriesKey)).forEach(function(item) {
      edges.push(item);
    });
  return edges;
}

_public.getAtlasAndVendorEdgesBySeries = function(mapSeries) {
  var mapsEdges = [];
  _public.getAllEdgesBySeriesAndEdge(mapSeries, mapAtlasPaths.name()).forEach(function(item) {
    mapsEdges.push(item);
  });
  _public.getAllEdgesBySeriesAndEdge(mapSeries, mapVendorPaths.name()).forEach(function(item) {
    mapsEdges.push(item);
  });
  return mapsEdges;
}

_public.getMapBaseItemTypeID = function(itemID){
  if(itemID.split("/")[0] === "BaseItemTypes") {
    return itemID;
  }
  var currentIndex = null;
  var mapDetailsVertexIDs = _public.mapDetailsIDs(itemID);
  var isBaseItemTypes = mapDetailsVertexIDs.some(function(vertexID, index) {
    currentIndex = index;
    return vertexID.split("/")[0] === "BaseItemTypes";
  });
  if(isBaseItemTypes) {
    return mapDetailsVertexIDs[currentIndex];
  }
  return;
}

_public.formatMapDetails = function(mapDetails) {
  var output = {
    "id":   mapDetails.BaseItemTypes._id,
    "Name": mapDetails.BaseItemTypes.Name,
    "Tier": mapDetails.Maps.Tier
  };
  return output
}

_public.formatMapDetailsWithPosition = function(mapDetails) {
  var output = {
    "id":   mapDetails.BaseItemTypes._id,
    "Name": mapDetails.BaseItemTypes.Name,
    "Tier": mapDetails.Maps.Tier
  };
  if("AtlasNode" in mapDetails) {
    output["position"] = {
      "x": mapDetails.AtlasNode.X,
      "y": mapDetails.AtlasNode.Y
    };
  }
  return output
}

_public.formatEdgeDetails = function(mapEdge) {
  var output = {
      "id": mapEdge._id,
      "source": _public.getMapBaseItemTypeID(mapEdge._from),
      "target": _public.getMapBaseItemTypeID(mapEdge._to),
      "type": mapEdge.type
  };
  return output;
}

_public.formatCytoscapeJSNode = function(mapDetails){
  var data = _public.formatMapDetails(mapDetails);
  var output = {
    "data": data,
    "group": "nodes"
  };
  if("AtlasNode" in mapDetails) {
    output["position"] = {
      "x": mapDetails.AtlasNode.X,
      "y": mapDetails.AtlasNode.Y
    };
  }
  return output;
}

_public.formatD3JSNode = function(mapDetails){
  var output =  _public.formatMapDetails(mapDetails);
  if("AtlasNode" in mapDetails) {
    output["fx"] = mapDetails.AtlasNode.X;
    output["fy"] = mapDetails.AtlasNode.Y;
  }
  return output;
}

_public.formatSigmaJSNode = function(mapDetails) {
  var output =  _public.formatMapDetails(mapDetails);
  output["label"] =  mapDetails.BaseItemTypes.Name;
  if("AtlasNode" in mapDetails) {
    output["x"] = mapDetails.AtlasNode.X;
    output["y"] = mapDetails.AtlasNode.Y;
  }
  return output;
}

_public.formatCytoscapeJSEdge = function(mapEdge){
  var data = _public.formatEdgeDetails(mapEdge);
  return {
    "data": data,
    "group": "edges"
  }
}

_public.formatD3JSEdge = function(mapEdge) {
  var output = _public.formatEdgeDetails(mapEdge);
  return output;
}

_public.formatSigmaJSEdge = function(mapEdge) {
  var output = _public.formatEdgeDetails(mapEdge);
  return output;
}

exports.poe = _public;
