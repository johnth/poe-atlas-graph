<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Path of Exile Atlas graph](#path-of-exile-atlas-graph)
    - [To do](#to-do)
- [Working](#working)
    - [Run ArangoDB](#run-arangodb)
        - [JavaScript](#javascript)
        - [REST](#rest)
        - [AQL queries](#aql-queries)
- [Generate data with PyPoE](#generate-data-with-pypoe)
    - [Data](#data)
        - [Interesting data files:](#interesting-data-files)
        - [Get json data file from Content.ggpk with PyPoE](#get-json-data-file-from-contentggpk-with-pypoe)
        - [Split and process json data](#split-and-process-json-data)
        - [ArangoDB import](#arangodb-import)
        - [ArangoDB create graph](#arangodb-create-graph)
    - [Cytoscape.js](#cytoscapejs)
    - [Images](#images)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Path of Exile Atlas graph

## To do
- Add user & account nodes
  - With edges user-(on account)-(has completed)-map
  - With edges user-(on account)-(has shaped)-map
  - With shaper orb attributes
  - With divine vessle attributes
- Add shaped maps and edges

# Working
## Run ArangoDB
see [arango_graphs] for documentation
[arango_graphs]:https://docs.arangodb.com/3.2/Manual/Graphs/GeneralGraphs/index.html

```bash
mkdir arangodb

sudo docker pull arangodb

sudo docker run \
  --name arangodb-instance \
  --detach \
  --tty \
  --rm=true \
  --env="ARANGO_ROOT_PASSWORD=password" \
  --env="ARANGO_STORAGE_ENGINE=rocksdb" \
  --expose=8529 \
  --volume="/home/john/arangodb:/var/lib/arangodb3" \
  arangodb

sudo docker inspect \
  --format '{{ .NetworkSettings.IPAddress }}' \
  arangodb-instance

sudo docker exec --interactive --tty arangodb-instance bash
```

### JavaScript

```bash
arangosh --server.database poe --server.username poe --server.password poe
```

```js
//Get vendor upgrade of a map

var graph_module = require("@arangodb/general-graph");

var maps = db._collection("Maps");
var baseItemTypes = db._collection("BaseItemTypes");
var worldAreas = db._collection("WorldAreas");
var atlasNode = db._collection("AtlasNode");

var mapsEdgesMapUpgradeBaseItemTypesKey = db._collection("Maps-edges-MapUpgrade_BaseItemTypesKey");
var mapsEdgesHigherTierMapsBaseItemTypesKeys = db._collection("Maps-edges-HigherTierMaps_BaseItemTypesKeys");

var atlas = graph_module._graph("atlas");

//Get id of baseitem by name
var startMapQuery = baseItemTypes.byExample({Name: "Lookout Map"});
var startMap = startMapQuery.next();

//find MapSeriesKey (index + 1) for "War for the Atlas" 
db._collection("MapSeries").byExample({Name: "War for the Atlas"}).toArray();

//count tier 2 maps in War of the Atlas
var mapsFilter = {Tier:2, MapSeriesKey:4};
maps.byExample(mapsFilter).count();

//count War of the Atlas maps per tier
Array.from(Array(17).keys()).forEach(function(i) {
  var tier = i + 1;
  var mapsFilter = {Tier: tier, MapSeriesKey: 4};
  var tierCount = maps.byExample(mapsFilter).count();
  print("Tier:" + tier + ": " + tierCount + " maps");
});

//Get all details for a map
function mapDetailsIDs(startNodeID) {
  var edgeDefinitionNamesMapDetails = [
    "AtlasNode-edges-WorldAreasKey",
    "Maps-edges-Regular_WorldAreasKey",
    "Maps-edges-Unique_WorldAreasKey",
    "Maps-edges-BaseItemTypesKey"
  ];

  var edgeMapDetailsOptions = {
    "direction": "any",
    "edgeCollectionRestriction": edgeDefinitionNamesMapDetails,
    "minDepth": 1,
    "maxDepth": 3
  };

  return atlas._neighbors(startNodeID, edgeMapDetailsOptions);
}

function mapDetailsObject(documentIDs) {
  var mapDetails = {}
  documentIDs.forEach(function(documentid) {
    var collectionName = documentid.split("/",1)[0];
    mapDetails[collectionName] = db._document(documentid);
  })
  return mapDetails;
}

function mapDetailsEssentials(mapDetails) {
  var output = {
    "Name": mapDetails.BaseItemTypes.Name,
    "Tier": mapDetails.Maps.Tier,
    "AreaLevel": mapDetails.WorldAreas.AreaLevel
  };
  if("AtlasNode" in mapDetails) {
    output["position"] = {
      "x": mapDetails.AtlasNode.X,
      "y": mapDetails.AtlasNode.Y
    };
  }
  return output;
}

//get brief details for all tier 2 maps
var mapsFilter = {Tier:2};

var mapsTemp = []
maps.byExample(mapsFilter).toArray().forEach(function(document) {
  mapsTemp.push(mapDetailsObject(mapDetailsIDs(document._id)));
});
mapsTemp.forEach(function(map) {
  print(mapDetailsEssentials(map));
})

//Get vendor upgrades of map, requires a map ID
var startMapObject = mapDetailsObject(mapDetailsIDs(startMap._id));
var edgeVendorEdges = [
  "Maps-edges-MapUpgrade_BaseItemTypesKey"
]

var edgeVendorOptions = {
  "direction": "outbound",
  "edgeCollectionRestriction": edgeVendorEdges,
  "minDepth": 1,
  "maxDepth": 1,
}

var exampleVendorDetails = mapDetailsObject(
  mapDetailsIDs(
    atlas._neighbors(
      startMapObject.Maps._id, edgeVendorOptions
    )
  )
);
print("Vendor upgrade for: " + startMapObject.BaseItemTypes.Name);
print(exampleVendorDetails.BaseItemTypes.Name);

//Get atlas neighbors of map
var edgeAtlasEdges = [
  "Maps-edges-HigherTierMaps_BaseItemTypesKeys"
]

var edgeAtlasOptions = {
  "direction": "any",
  "edgeCollectionRestriction": edgeAtlasEdges,
  "minDepth": 1,
  "maxDepth": 1,
}

var exampleAtlasNeighbors = atlas._neighbors(
  startMapObject.Maps._id, edgeAtlasOptions
);
var exampleAtlasNeighborDetails = [];
exampleAtlasNeighbors.forEach(function(itemID) {
  exampleAtlasNeighborDetails.push(
    mapDetailsObject(mapDetailsIDs(itemID))
  )
});

print("Atlas neighbors for: " + startMapObject.BaseItemTypes.Name);
exampleAtlasNeighborDetails.forEach(function(item) {
  print(item.BaseItemTypes.Name)
});

// Find atlas neighbors by tier
db._collection("Maps").byExample({Tier:3, MapSeriesKey: 4}).toArray().forEach(function(map) {
  var tempDetails = documentIDsToObject(mapDetailsIDs(map));
  var atlasNeighbors = mapAtlasNeighborIDs(map._id);
  var printString = "t" + tempDetails.Maps.Tier + " neighbors: " + atlasNeighbors.length;
  printString += " " + tempDetails.BaseItemTypes.Name;
  print(printString);
  atlasNeighbors.forEach(function(neighborID) {
    var tempNeighborDetails = documentIDsToObject(mapDetailsIDs(neighborID));
    print(" t" + tempNeighborDetails.Maps.Tier + " " + tempNeighborDetails.BaseItemTypes.Name);
  });
});

//cytoscape.js outputs

function formatCytoscapeJSNode(mapDetails){
  //var idSplit = mapDetails.BaseItemTypes.Id.split("/");
  //var id = idSplit[idSplit.length-1];
  var id = mapDetails.BaseItemTypes._id;
  var data = Object.assign(
    {"id": id},
    mapDetails
  );
  var output = {
    "data": data,
    "group": "nodes"
  };
  if("AtlasNode" in mapDetails) {
    output["position"] = {
      "x": mapDetails.atlasnode.X,
      "y": mapDetails.atlasnode.Y
    };
  }
  return output;
}

function formatCytoscapeJSEdge(mapEdge){
  var id = mapEdge._from + "-to-" + mapEdge._to;
  var data = Object.assign({
      "id": id,
      "source": mapEdge._from,
      "target": mapEdge._to,
    },
    mapEdge
  );
  return {
    "data": data,
    "group": "edges"
  }
}

//output all maps details for each item in baseitem
var mapsCytoscapeJSNodes = []
db._collection("Maps").byExample({MapSeriesKey: 4}).toArray()forEach(function(item) {
  var mapDetails = mapDetailsObject(mapDetailsIDs(item._id));
  mapsCytoscapeJSNodes.push(formatCytoscapeJSNode(mapDetails));
})


//outout all edges for each edge in atlas and vendor
var mapsCytoscapeJSEdges = []
db._collection("Maps-edges-HigherTierMaps_BaseItemTypesKeys").toArray().forEach(function(item) {
  mapsCytoscapeJSEdges.push(formatCytoscapeJSEdge(item));
})
db._collection("Maps-edges-MapUpgrade_BaseItemTypesKey").toArray().forEach(function(item) {
  mapsCytoscapeJSEdges.push(formatCytoscapeJSEdge(item));
})

//Find map atlas path neighbors
db._collection("Maps").byExample({Tier:5, MapSeriesKey:4}).toArray().forEach(
function(map) {
  var mapObject = documentIDsToObject(mapDetailsIDs(map));
  print(mapObject.BaseItemTypes.Name);
  var startNodeID = mapObject.Maps._id;

  var edgeMapDetailsEdges = [
    "Maps-edges-HigherTierMaps_BaseItemTypesKeys"
  ];

  var edgeMapDetailsOptions = {
    "direction": "any",
    "edgeCollectionRestriction": ["Maps-edges-HigherTierMaps_BaseItemTypesKeys"],
    "minDepth": 1,
    "maxDepth": 1
  };
  var atlasNeighbors = atlas._neighbors(startNodeID, edgeMapDetailsOptions);
  atlasNeighbors.forEach(
  function(neighborMap) {
    var neighborMapObject = documentIDsToObject(mapDetailsIDs(neighborMap));
    print(neighborMapObject.Maps.Tier.toString() + " " + neighborMapObject.BaseItemTypes.Name);
  });
});
```

### REST

Not updated

```bash
token=$(curl --data '{"username":"root","password":"password"}' http://localhost:8529/_open/auth | jq -r .jwt)
curl --header "Authorization: bearer $token" \
'http://localhost:8529/_api/edges/map_relations_atlas?vertex=map/mesa_map' | jq

curl --header "Authorization: bearer $token" \
-X POST --data-binary '{
"startVertex":"map/burial_chambers_map",
"graphName":"atlas",
"direction":"inbound",
"uniqueness": {"vertices": "global", "edges": "global"}
}' \
'http://localhost:8529/_api/traversal' | jq

curl --header "Authorization: bearer $token" \
-X POST --data-binary '{
"startVertex":"map/burial_chambers_map",
"graphName":"atlas",
"direction":"inbound",
"uniqueness": {"vertices": "global", "edges": "global"}
}' \
'http://localhost:8529/_api/traversal' | jq '.result.visited.vertices[].name'
```

```bash
curl --header "Authorization: bearer $token" \
http://localhost:8529/_api/document/map/racecourse_map | jq '.'

curl -X PATCH --header "Authorization: bearer $token" \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-binary '{
"count":1,
"completed":false,
"completed_bonus":false
}' 'http://localhost:8529/_db/_system/_api/document/map/racecourse_map?mergeObjects=false' | jq '.'

curl -X POST --header "Authorization: bearer $token" \
--data-binary '{
"query":"FOR m IN map FILTER m.tier == 6 && m.count < 1 && m.completed != true AND m.unique != true RETURN m.name"
}' \
'http://localhost:8529/_api/cursor' | jq '.result[]'

curl -X POST --header "Authorization: bearer $token" \
--data-binary '{
"startVertex":"map/quarry_map",
"graphName":"atlas",
"direction":"inbound",
"uniqueness": {"vertices": "global", "edges": "global"},
"filter": "if (!vertex.count) {return \"exclude\";}return;"
}' \
'http://localhost:8529/_api/traversal' | jq '.'

maps=(
$(curl -X POST --header "Authorization: bearer $token" \
--data-binary '{
"query":"FOR m IN map FILTER m.tier <= 5 AND m.unique != true RETURN m._key"
}' \
'http://localhost:8529/_api/cursor' | jq --raw-output '.result[]')
)

for map in "${maps[@]}"; do
  curl --silent --output /dev/null -X PATCH --header "Authorization: bearer $token" \
  --header 'Content-Type: application/json' \
  --header 'Accept: application/json' \
  --data-binary '{
  "completed":true,
  "completed_bonus":true
  }' "http://localhost:8529/_db/_system/_api/document/map/${map}?mergeObjects=false"
done

curl --silent --output /dev/null -X PATCH --header "Authorization: bearer $token" \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-binary '{
"completed":null,
"completed_bonus":null
}' "http://localhost:8529/_db/_system/_api/document/map/burial_chambers_map?mergeObjects=false"

curl -X POST --header "Authorization: bearer $token" \
--data-binary '{
"query":"FOR m IN map FILTER m.tier <= 6 && m.count < 1 && m.completed != true AND m.unique != true SORT m.tier ASC RETURN m.name"
}' \
'http://localhost:8529/_api/cursor' | jq '.result[]'
```

```bash
owned_incompleted_maps=(
strand_map
racecourse_map
spider_forest_map
canyon_map
thicket_map
)
for map in "${owned_incompleted_maps[@]}"; do
  curl --silent --output /dev/null -X PATCH --header "Authorization: bearer $token" \
  --header 'Content-Type: application/json' \
  --header 'Accept: application/json' \
  --data-binary '{
  "count":1
  }' "http://localhost:8529/_db/_system/_api/document/map/${map}?mergeObjects=false"
done

maps_required=(
$(curl -X POST --header "Authorization: bearer $token" \
--data-binary '{
"query":"FOR m IN map FILTER m.tier <= 6 && m.count < 1 AND m.completed != true AND m.unique != true SORT m.tier ASC RETURN m._key"
}' \
'http://localhost:8529/_api/cursor' | jq --raw-output '.result[]')
)

for map in "${maps_required[@]}"; do
  curl --silent -X POST --header "Authorization: bearer $token" \
  --data-binary '{
    "startVertex":"'"map/$map"'",
    "graphName":"atlas",
    "direction":"any",
    "uniqueness": {"vertices": "global", "edges": "global"},
    "filter": "if (vertex.tier > 6 || !vertex.completed) {return \"exclude\";}return;",
    "maxDepth": 1,
    "edgeCollection":"map_relations_atlas"
  }' \
  'http://localhost:8529/_api/traversal' | jq '.result.visited.vertices[].name'
done
```
### AQL queries

Not updated

In `arangosh`
```js
// Traditional join
db._query("FOR map IN map \
  FILTER map.Tier == 5 \
  FOR item IN baseitem \
    FILTER map.BaseItemTypesKey == item.index \
RETURN {'Name': item.Name, 'Tier': map.Tier}").toArray();
// Can use RETURN MERGE(item,map)


// Get relevant map details in Cytoscape.js style output
db._query("FOR startmap IN baseitem \
FILTER startmap.Name LIKE '%Burial Chambers%' \
LIMIT 1 \
LET mapdetails = ( \
  FOR vertex, edge, path \
  IN 0..3 \
  ANY \
  startmap \
  GRAPH 'atlas' \
  OPTIONS { \
    uniqueVertices:'global', \
    uniqueEdges:'path', \
    bfs:true \
  } \
  FILTER \
  path.edges[-1].type == 'atlasnode references worldarea' \
  RETURN ({'baseitem': path.vertices[0], 'map': path.vertices[1], 'worldarea': path.vertices[2], 'atlasnode': path.vertices[3]}) \
) \
RETURN { \
  'data': { \
  'Name': mapdetails[0].baseitem.Name, \
  'Tier': mapdetails[0].map.Tier, \
  'AreaLevel': mapdetails[0].worldarea.AreaLevel, \
  'DropLevel': mapdetails[0].baseitem.DropLevel, \
  'id': mapdetails[0].baseitem.index \
  }, \
  'position': { \
    'x': mapdetails[0].atlasnode.X, \
    'y': mapdetails[0].atlasnode.Y \
  } \
} \
").toArray();


\\find maps adajacent or vendor upgrade for burial chambers
db._query("FOR vertex, edge, path \
IN 0..1 \
INBOUND \
'map/burial_chambers_map' \
map_relations_vendor, \
ANY map_relations_atlas \
OPTIONS { \
uniqueVertices:'global', \
uniqueEdges:'path', \
bfs:true \
} \
RETURN [vertex.name,vertex.tier]")

db._query(" \
LET atlas = (FOR e IN map_relations_atlas RETURN e) \
LET vendor = (FOR e IN map_relations_vendor RETURN e) \
FOR m in map \
FILTER m.id NOT IN atlas._to \
RETURN m.name")

//Maps that can't be vendor created
db._query("FOR m in map \
FILTER m._id \
NOT IN (FOR e in map_relations_vendor RETURN e._to) \
SORT m.tier ASC \
RETURN m.name").toArray()

//Maps that aren't connected on the atlas
db._query("FOR m in map \
FILTER \
m._id NOT IN (FOR e in map_relations_atlas RETURN e._to) \
&& m._id NOT IN (FOR e in map_relations_atlas RETURN e._from) \
SORT m.tier ASC \
RETURN m.name").toArray()
```

# Generate data with PyPoE

[PyPoE is a tool to extract information from Content.ggpk](https://github.com/OmegaK2/PyPoE)  
[PyPoE Documentation](http://omegak2.net/poe/PyPoE/CLI/dat.html#dat-json)  

## Data

### Interesting data files:
- AtlasNode
- Map  
  where `MapSeriesKey == 4` is PoE v3.1.0 _War for the Atlas_  
  jq filter `select(.MapSeriesKey" == 4 )`  
  jq filter for prior to object-ifying, select the key name index `| select(.[($headernames | index("MapSeriesKey"))] == 4 )`
- WorldAreas  
  where `Id contains("MapWorlds")` is PoE v3.1.0 _War for the Atlas_  
  jq filter `select(.Id) | contains("MapWorlds")`
- BaseItemTypes  
  where `Id contains("MapWorlds")` is PoE v3.1.0 _War for the Atlas_

### Get json data file from Content.ggpk with PyPoE
```bash
mkdir -p /tmp/{py,}poe
pypoe_exporter config set temp_dir /tmp/pypoe
pypoe_exporter config set out_dir /tmp/poe
pypoe_exporter config set ggpk_path /mnt/poe/Path_of_Exile/Content.ggpk
pypoe_exporter setup perform
pypoe_exporter dat json /tmp/poe/data.json
```

### Split and process json data

- Individual pretty files for each filename
- Convert arrays to objects
- Add index: arrayindex

```bash
cat <<EOF > /tmp/poe_process_data.jq
.[] # select everything
| [.header[].name] as \$headernames # store names of keys for later use
| .data as \$a # store array of all items
| (\$a|keys[]) as \$i # store array index as i
| (\$a[\$i]+[\$i]) # push index to end of array for each item
| with_entries( # create an object from array
  if .key < (\$headernames | length)
  # if the array index is less than the number of keys listed in the header
  # this is a value of a key defined in the header
  then .key = \$headernames[.key] 
  # set this value for
  # otherwise, this must be the index we appended
  else .key = "index" 
  # set this as value for key index
end)
| . += {"_key": ( .index | tostring )} # store index as _key (for ArangoDB)
EOF

cat <<EOF > /tmp/poe_process_header.jq
.[]
| .header as \$a
| (\$a|keys[]) as \$i # store array index as i
| \$a[\$i] # iterate through headers
| . += {"index": \$i} # store index in each header
| . += {"_key": ( .index | tostring )} # store index as _key (for ArangoDB)
EOF

cat <<EOF > /tmp/poe_process_edges.jq
.[]
| ( .filename | split(".dat")[0] )
as \$fn # store the name of the file, so we know the edge source
| .header
as \$ah # store the header details, so we can use them later
| [ # want array of items
(\$ah|keys[])
as \$ih # store array index as ih and iterate
| \$ah[\$ih] # iterate through headers
| select(.key) # header has a foreign key
| \$ih
] # want array of (indicies for) columns that contain a foreign key
as \$foreignkeyindicies
| [ # want array of edge relationships for each relationship type
(\$foreignkeyindicies|keys[])
as \$foreignkeyiterator # iterate through columns that contain a fk
| [
.data
as \$a # store array of all items to get iterator
| (\$a|keys[])
as \$i # store array index as i and iterate
| (\$a[\$i]+[\$i]) # push index to end of array for each item
# item index needed for edge source
| \$foreignkeyindicies[\$foreignkeyiterator]
as \$fki # the index of the column
| \$a[\$i][\$fki]
as \$fktarget # the value of the item attribute in this foreign key column
| select(\$fktarget) # only want items that give an fk
| { "_from": ( \$fn + "/" + ( \$i | tostring )) } # arangodb style edge source (from this collection (fn)/_key)
| if ( \$fktarget | type ) ==  "array" then # if there are multiple targets
  # then produce an object for each
  . += { "_to": (( \$ah[\$fki].key ) | split(".dat")[0] + "/" + ( \$fktarget[] | tostring ))}
else
  . += { "_to": (( \$ah[\$fki].key ) | split(".dat")[0] + "/" + ( \$fktarget | tostring ))}
end
| .+= { "type": \$ah[\$fki].name }
]]
EOF

jq -c '.=(.[] | [.])' /tmp/poe/data.json | \
while read -r json; do
  filename_ggpk=$(jq -r '.[].filename' <<< "$json") && \
  filename="${filename_ggpk%\.dat}" && (\
    echo "starting: $filename"
    nodes=$(jq -cf /tmp/poe_process_data.jq <<< "$json")
    if [[ -n "$nodes" ]]; then
      echo "$nodes" > /tmp/poe/split/${filename}.json;
    fi
    echo "nodes done";
    headers=$(jq -cf /tmp/poe_process_header.jq <<< "$json")
    if [[ -n "$headers" ]]; then
      echo "$headers" > /tmp/poe/split/${filename}-header.json;
    fi
    echo "headers done";
    edges=$(jq -cf /tmp/poe_process_edges.jq <<< "$json")
    if [[ -n "$edges" ]]; then
      echo "$edges" | jq -c '.=(.[] | [.])' | while read -r edgesjson; do
        edgestype=$(jq -r '.[0][0].type' <<< "$edgesjson");
        echo "$edgestype"
        edgessplit=$(jq -c '.[][]' <<< "$edgesjson");
        if [[ -n "$edgessplit" ]]; then
          echo "$edgessplit" > /tmp/poe/split/${filename}-edges-${edgestype}.json;
        fi
      done
    fi
    echo "edges done";
  )
done
```

These files can then be imported to arango as is  
If wanted, the json data can be filtered to only map relevant data using the filters detailed above.

### ArangoDB import

```bash
arangosh --server.database poe --server.username poe --server.password poe
```

```js
//remove all collections name not starting with _
db._collections().forEach(function(c) {var re=/^_/; if(! (c.name().search(re)>-1)) { c.drop() }})
```

```bash
arangoimpargs=(
--server.database poe
--server.username poe
--server.password poe
--overwrite true
--create-collection true
--type jsonl
)
for i in *.json; do
  localarangoimpargs=("${arangoimpargs[@]}")
  if [[ "$i" == *"-edges"* ]]; then
    localarangoimpargs+=(--create-collection-type edge)
  fi
  arangoimp "${localarangoimpargs[@]}" --file "$i" --collection "${i%\.json}";
done
```

### ArangoDB create graph

Run arangosh
```bash
arangosh --server.database poe --server.username poe --server.password poe
```

Javascript to execute in arangosh
```js
var graph_module = require("@arangodb/general-graph");

//all relations
var edgeDefinitions = []
db._collections().forEach(function(c) {
  var reEdges=/-edges/;
  if((c.name().search(reEdges)>-1)) {
    var re=/-edges-(.*)/;
    var temp = c.name().match(re)[1];
    var sampleEdge = c.any();
    if(sampleEdge) {
      var edgeDefinitionTemp = graph_module._relation(
        c.name(),
        sampleEdge._to.split("/")[0],
        sampleEdge._from.split("/")[0]
      );
      edgeDefinitions.push(edgeDefinitionTemp);
    }
  }
});

//names of edge definitions wanted in graph
var edgeDefinitionNamesMapDetails = [
  "AtlasNode-edges-WorldAreasKey",
  "Maps-edges-Regular_WorldAreasKey",
  "Maps-edges-Unique_WorldAreasKey",
  "Maps-edges-BaseItemTypesKey"
];

var edgeDefinitionNamesAtlas = edgeDefinitionNamesMapDetails + [
  "Maps-edges-HigherTierMaps_BaseItemTypesKeys",
  "Maps-edges-MapUpgrade_BaseItemTypesKey"
];

var edgeDefinitionsAtlas = [];
edgeDefinitions.forEach(function(ed) {
  //if name of edge definition is in wanted edge definitions
  if(edgeDefinitionNamesAtlas.indexOf(ed.collection) > -1) {
    edgeDefinitionsAtlas.push(ed);
  }
});

var atlas = graph_module._create("atlas", edgeDefinitionsAtlas);
```

## Cytoscape.js

```bash
jq ' .
| [
[.[].header[].name] as $headernames
| .[].data as $a
| ($a|keys[]) as $i
| ($a[$i]+[$i])
| with_entries(
  if .key < ($headernames | length)
    then .key = $headernames[.key]
  else .key = "index"
  end
)
| . += {"_key": .index}
| . += {"id": .index}
| {"data": .}
| . += {"position": {"x": .data.X, "y": .data.Y}}
]
' /tmp/poe/split/AtlasNode.dat.json | tee /tmp/poe/graph/poe_atlas_nodes.json
```

```js
elements:
   fetch('./poe_atlas_nodes.json').then(
        function( res ){
            return res.json();
        })
```

## Images

The Atlas map is stored in /Art/2DArt/Atlas/Atlas.dds

1. Open `py_poe`
2. Check Uncompress DDS files... in `Misc/Settings`
3. Extract Atlas.dds
4. Convert DDS to PNG:  
   `convert Atlas.dds atlas.png`
